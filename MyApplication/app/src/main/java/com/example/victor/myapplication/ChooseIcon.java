package com.example.victor.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ChooseIcon extends AppCompatActivity {

    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_icon);

        lista = findViewById(R.id.listachoose);

        int[] imagenes = {R.drawable.chibiumaru, R.drawable.chibikuroko, R.drawable.chibipikachu,R.drawable.chibiasuna,R.drawable.chibizelda,R.drawable.chibiluffy};

        lista.setAdapter(new CustomAdapter(this,imagenes));
    }

    public void ResultEnd(int i){
        setResult(i);
        finish();
    }
}
