package com.example.victor.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Victor on 10/01/2018.
 */

public class CustomAdapter extends BaseAdapter {
    private Context context;
    private int[]imageID;
    private static LayoutInflater inflater=null;

    public CustomAdapter(ChooseIcon chooseIcon, int[] imagenes) {
        this.context = chooseIcon;
        this.imageID = imagenes;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return imageID.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Holder{
        ImageView image;
        TextView nombre;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder;
        holder = new Holder();
        View imagenes;
        imagenes = inflater.inflate(R.layout.row,null);
        holder.image = (ImageView)imagenes.findViewById(R.id.img);
        holder.nombre = (TextView)imagenes.findViewById(R.id.nombre);
        holder.image.setImageResource(imageID[i]);

        if(i==0){
            holder.nombre.setText("Umaru");
        }else if (i==1){
            holder.nombre.setText("Kuroko");
        }else if(i==2){
            holder.nombre.setText("Pikachu");
        }else if(i==3){
            holder.nombre.setText("Asuna");
        }else if(i==4){
            holder.nombre.setText("Zelda");
        }else if(i==5){
            holder.nombre.setText("Luffy");
        }

        imagenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChooseIcon choose = (ChooseIcon)context;
                choose.ResultEnd(i);
                if(i==0){
                    Toast.makeText(choose, "¡Has elegido a Umaru!", Toast.LENGTH_SHORT).show();
                }else if (i==1){
                    Toast.makeText(choose, "¡Has elegido a Kuroko!", Toast.LENGTH_SHORT).show();
                }else if(i==2){
                    Toast.makeText(choose, "¡Has elegido a Pikachu!", Toast.LENGTH_SHORT).show();
                }else if(i==3){
                    Toast.makeText(choose, "¡Has elegido a Asuna!", Toast.LENGTH_SHORT).show();
                }else if(i==4){
                    Toast.makeText(choose, "¡Has elegido a Zelda!", Toast.LENGTH_SHORT).show();
                }else if(i==5){
                    Toast.makeText(choose, "¡Has elegido a Luffy!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return imagenes;
    }
}
