package com.example.victor.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Registro extends AppCompatActivity {

    private EditText ETjugadorUno;
    private EditText ETjugadorDos;
    private ImageButton IBjugadorUno;
    private ImageButton IBjugadorDos;
    private Button BTjugar;
    SharedPreferences prefsUno;
    SharedPreferences prefsDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        prefsUno= getSharedPreferences("jugadorUno", Context.MODE_PRIVATE);
        prefsDos = getSharedPreferences("jugadosDos", Context.MODE_PRIVATE);
        prefsUno.edit().clear();
        prefsDos.edit().clear();
        ETjugadorUno = findViewById(R.id.ETjugadorUno);
        ETjugadorDos = findViewById(R.id.ETjugadorDos);
        IBjugadorUno = findViewById(R.id.IBjugadorUno);
        IBjugadorDos = findViewById(R.id.IBjugadorDos);
        BTjugar = findViewById(R.id.Play);

        IBjugadorUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(),ChooseIcon.class),200);
            }
        });

        IBjugadorDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(),ChooseIcon.class),201);
            }
        });

        BTjugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usernameUno = ETjugadorUno.getText().toString();
                String usernameDos = ETjugadorDos.getText().toString();
                int idUno = getSharedPreferences("jugadorUno",Context.MODE_PRIVATE).getInt("IconUno",-1);
                int idDos = getSharedPreferences("jugadosDos", Context.MODE_PRIVATE).getInt("IconDos",-1);

                if(usernameUno.isEmpty() || usernameDos.isEmpty()){
                    Toast.makeText(Registro.this, "Introduce los nombres de los jugadores", Toast.LENGTH_SHORT).show();
                }else if (usernameUno == usernameDos){
                    Toast.makeText(Registro.this, "¡El nombre de los jugadores no puede ser el mismo!", Toast.LENGTH_SHORT).show();
                }else if (idUno == idDos){
                    Toast.makeText(Registro.this, "¡El icono de los jugadores no puede ser el mismo!", Toast.LENGTH_SHORT).show();
                }else if (idUno== -1 ||idDos==-1 ){
                    Toast.makeText(Registro.this, "¡Tienes que seleccionar un icono!", Toast.LENGTH_SHORT).show();
                }else {
                    prefsUno.edit().putString("jugadorUno", usernameUno).commit();
                    prefsDos.edit().putString("jugadorDos", usernameDos).commit();
                    startActivity(new Intent (getApplicationContext(),Game.class));
                }

            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {



        if(requestCode==200){

            prefsUno.edit().putInt("IconUno",resultCode).commit();

            if(resultCode == 0){
                IBjugadorUno.setImageResource(R.drawable.chibiumaru);
            }else if (resultCode == 1){
                IBjugadorUno.setImageResource(R.drawable.chibikuroko);
            }else if (resultCode == 2){
                IBjugadorUno.setImageResource(R.drawable.chibipikachu);
            }else if (resultCode == 3){
                IBjugadorUno.setImageResource(R.drawable.chibiasuna);
            }else if (resultCode == 4){
                IBjugadorUno.setImageResource(R.drawable.chibizelda);
            }else if (resultCode == 5){
                IBjugadorUno.setImageResource(R.drawable.chibiluffy);
            }

        }
        else if (requestCode == 201){

            prefsDos.edit().putInt("IconDos",resultCode).commit();

            if(resultCode == 0){
                IBjugadorDos.setImageResource(R.drawable.chibiumaru);
            }else if (resultCode == 1){
                IBjugadorDos.setImageResource(R.drawable.chibikuroko);
            }else if (resultCode == 2){
                IBjugadorDos.setImageResource(R.drawable.chibipikachu);
            }else if (resultCode == 3){
                IBjugadorDos.setImageResource(R.drawable.chibiasuna);
            }else if (resultCode == 4){
                IBjugadorDos.setImageResource(R.drawable.chibizelda);
            }else if (resultCode == 5){
                IBjugadorDos.setImageResource(R.drawable.chibiluffy);
            }
        }


    }
}
