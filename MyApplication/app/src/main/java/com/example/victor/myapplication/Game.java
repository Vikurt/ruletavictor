package com.example.victor.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.Random;

public class Game extends AppCompatActivity {

    private ImageView avatarUno, avatarDos;
    private TextView usernameUno, usernameDos, numRonda, frase, pista, puntTotal, puntParcial,dinero;
    private Button tirar, aceptar, resolver;
    private String nombreUno, nombreDos;
    private int identUno, identDos;
    private EditText letra;
    private int[] imagenes = {R.drawable.chibiumaru, R.drawable.chibikuroko, R.drawable.chibipikachu,R.drawable.chibiasuna,R.drawable.chibizelda,R.drawable.chibiluffy};
    private String[] frases = {};
    private String[] pistas = {};
    private char[] frasesVacias, frasesLlenas;
    private boolean turnojuego;
    int i, parcialJ1, parcialJ2, totalJ1, totalJ2, numAciertos, numruleta;
    char letrachar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        avatarUno = findViewById(R.id.avatarUno);
        avatarDos = findViewById(R.id.avatarDos);

        usernameUno = findViewById(R.id.usernameUno);
        usernameDos = findViewById(R.id.usernameDos);
        numRonda = findViewById(R.id.round);
        frase = findViewById(R.id.frase);
        pista = findViewById(R.id.pista);
        puntTotal = findViewById(R.id.total);
        puntParcial = findViewById(R.id.parcial);
        dinero = findViewById(R.id.dinero);

        tirar = findViewById(R.id.tirarJuego);
        aceptar = findViewById(R.id.aceptarJuego);
        resolver = findViewById(R.id.resolver);

        letra = findViewById(R.id.letra);

        SharedPreferences j1, j2;

        turnojuego = true;
        numAciertos=0;

        j1 = getSharedPreferences("jugadorUno", Context.MODE_PRIVATE);
        j2 = getSharedPreferences("jugadorDos", Context.MODE_PRIVATE);

        identUno = j1.getInt("IconUno", 0);
        identDos = j2.getInt("IconDos", 0);

        nombreUno = j1.getString("jugadorUno", "Player1");
        nombreDos = j2.getString("jugadorDos", "Player2");

        avatarUno.setImageResource(imagenes[identUno]);
        avatarDos.setImageResource(imagenes[identDos]);

        usernameUno.setText(nombreUno);
        usernameDos.setText(nombreDos);

        Random randomGenerator = new Random();
        int numerito = randomGenerator.nextInt(3);

        pista.setText(getResources().getStringArray(R.array.pistas)[numerito]);
        final String Panel = getResources().getStringArray(R.array.frases)[numerito];

        frasesVacias = Panel.toCharArray();
        frasesLlenas = Panel.toCharArray();

        for(i=0; i<frasesVacias.length;i++){
            if(frasesVacias[i] != ' '){
                    frasesVacias[i] = '_';
            }
        }
        i = i-1;
        frase.setText(new String(frasesVacias));

        tirar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(turnojuego == true){
                    Random ruletaRandom = new Random();
                    numruleta = ruletaRandom.nextInt(100);
                    if (numruleta < 10){
                        dinero.setText("¡Quiebra!");
                        parcialJ1 = 0;
                        puntParcial.setText("PARCIAL:" +parcialJ1);
                        ChangePlayer(false);
                    }else dinero.setText(String.valueOf(numruleta));
                }else if (turnojuego == false){
                    Random ruletaRandom = new Random();
                    numruleta = ruletaRandom.nextInt(100);
                    if (numruleta < 10){
                        dinero.setText("¡Quiebra!");
                        parcialJ2 = 0;
                        puntParcial.setText("PARCIAL: "+parcialJ2);
                        ChangePlayer(true);
                    }else dinero.setText(String.valueOf(numruleta));
                }

            }
        });

        resolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText ETresolver = new EditText(Game.this);
                ETresolver.setInputType(InputType.TYPE_CLASS_TEXT);
                final AlertDialog.Builder alertaresolver = new AlertDialog.Builder(Game.this);

                alertaresolver.setMessage("Resuelve el panel").setView(ETresolver).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(ETresolver.getText().toString().equals(Panel)){
                            if(turnojuego == true){
                                totalJ1 = totalJ1 + parcialJ1 +150;
                                puntTotal.setText("TOTAL: "+totalJ1);
                            }else if(turnojuego == false){
                                totalJ2 = totalJ2 + parcialJ2 +150;
                                puntTotal.setText("TOTAL: "+totalJ2);
                            }

                            if(totalJ1 > totalJ2){
                                startActivity(new Intent(getApplicationContext(),ResultadoVictoria.class));
                            }else if(totalJ1 < totalJ2){
                                startActivity(new Intent(getApplicationContext(),ResultadoVictoria2.class));
                            }else if (totalJ1 == totalJ2){
                                startActivity(new Intent(getApplicationContext(),ResultadoEmpate.class));
                            }
                        }

                        if(ETresolver.getText().toString() != (Panel)){
                            if(turnojuego == true){
                                ChangePlayer(false);
                            }else  if (turnojuego == false){
                               ChangePlayer(true);
                            }

                            dialogInterface.dismiss();
                        }
                    }
                }).show();
            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                letrachar = letra.getText().toString().toCharArray()[0];
                String toMayus = String.valueOf(letrachar).toUpperCase();

                for(i=0; i<frasesVacias.length;i++){

                    if(frasesLlenas[i] == letrachar || frasesLlenas[i] == toMayus.toCharArray()[0]) {
                        frasesVacias[i] = letrachar;
                        numAciertos++;
                    }
                }

                        if(turnojuego == true){
                            parcialJ1 = parcialJ1+(numruleta*numAciertos);
                            puntParcial.setText("PARCIAL: "+parcialJ1);
                            ChangePlayer(false);
                        }else if (turnojuego == false){
                            parcialJ2 = parcialJ2+(numruleta*numAciertos);
                            puntParcial.setText("PARCIAL: "+parcialJ2);
                            ChangePlayer(true);

                        }else if (frasesLlenas[i] != letrachar || frasesLlenas[i] != toMayus.toCharArray()[0]){

                        }
                        if(turnojuego == false){
                            puntParcial.setText("PARCIAL: "+parcialJ2);
                            ChangePlayer(true);
                        }else if (turnojuego == true){
                            puntParcial.setText("PARCIAL: "+parcialJ1);
                            ChangePlayer(false);
                        }

                numAciertos = 0;
                frase.setText(new String(frasesVacias));
            }



        });


    }

    private void ChangePlayer(boolean turnojuego){
        this.turnojuego = turnojuego;

        if(turnojuego == true){
            avatarUno.setVisibility(View.VISIBLE);
            usernameUno.setVisibility(View.VISIBLE);

            avatarDos.setVisibility(View.INVISIBLE);
            usernameDos.setVisibility(View.INVISIBLE);
        }else if (turnojuego == false){
            avatarDos.setVisibility(View.VISIBLE);
            usernameDos.setVisibility(View.VISIBLE);

            avatarUno.setVisibility(View.INVISIBLE);
            usernameUno.setVisibility(View.INVISIBLE);
        }

    }
}
